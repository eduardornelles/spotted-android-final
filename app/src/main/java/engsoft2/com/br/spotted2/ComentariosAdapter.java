package engsoft2.com.br.spotted2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.LruCache;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.Profile;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by EduDornelles on 15/10/2015.
 */
public class ComentariosAdapter extends ArrayAdapter<Comentario>{

    View customView;
    ImageButton btn_like;
    ImageButton btn_dislike;
    ImageButton btn_denunciar;

    ComentariosAdapter(Context context, ArrayList<Comentario> comentarios){
        super( context, R.layout.comentario_row, comentarios );
    }

    public View getView(int position, View convertView, ViewGroup parent){

        LayoutInflater inflater = LayoutInflater.from(getContext());
        final Comentario comentario = getItem(position);

        customView = inflater.inflate(R.layout.comentario_row, parent, false);
        TextView text = (TextView) customView.findViewById( R.id.comentario_texto );
        TextView qteLike = (TextView) customView.findViewById(R.id.qte_like);
        TextView qteDislike = (TextView) customView.findViewById(R.id.qte_dislike);
        btn_like = (ImageButton) customView.findViewById(R.id.btn_like);
        btn_dislike = (ImageButton) customView.findViewById(R.id.btn_dislike);
        btn_denunciar = (ImageButton) customView.findViewById(R.id.btn_denunciar);
        text.setText( comentario.getTexto() );
        qteLike.setText( String.valueOf( comentario.getLikes() ) );
        qteDislike.setText( String.valueOf( comentario.getDislikes() ) );

        if( comentario.isDenunciado() == false ){
            btn_denunciar.setAlpha((float) 0.5);
        }

        if( comentario.isLiked() == false ){
            btn_like.setAlpha((float) 0.5);
        }

        if( comentario.isDisliked() == false ){
            btn_dislike.setAlpha((float) 0.5);
        }

        btn_like.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( comentario.isLiked() == false ) {
                    like( comentario );
                    comentario.setLiked(true);
                    comentario.setDisliked(false);
                }
            }

        });

        btn_dislike.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( comentario.isDisliked() == false ){
                    dislike(comentario );
                    comentario.setDisliked(true);
                    comentario.setLiked(false);
                }
            }

        });

        btn_denunciar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v){
                denunciar( comentario );
                comentario.setDenunciado(true);
            }

        });

        return customView;

    }

    public void like( Comentario comentario){
        String url = "http://noiadigital.com.br/spotted/public/api/comentario/like";

        try {

            final Comentario inner = comentario;

            Map<String, String>  params = new HashMap<>();
            params.put("usuario_id", String.valueOf( comentario.getUsuario_id() ) );
            params.put("comentario_id", String.valueOf(comentario.getId()));

            HttpRequest.getInstance( getContext() ).somePostRequestReturningString(url, params, new HttpRequest.SomeCustomListener<String>() {
                @Override
                public void getResult(String result) {
                    if (!result.isEmpty()) {

                        try {
                            JSONObject obj = new JSONObject( result );
                            Log.e("RESULT", obj.toString() );
                            inner.setLikes( obj.getInt("likes") );
                            inner.setDislikes(obj.getInt("dislikes"));
                            inner.setLiked(obj.getBoolean("liked"));
                            inner.setDisliked(obj.getBoolean("disliked"));
                            updateAdapter();
                            Toast.makeText(getContext(), "Você curtiu o comentário", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getContext(), "Não foi possível curtiu o comentário", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch( Exception e){
            Toast.makeText( getContext() , "Erro ao conectar ao servidor.", Toast.LENGTH_SHORT).show();
        }
    }

    public void dislike( Comentario comentario ){
        String url = "http://noiadigital.com.br/spotted/public/api/comentario/dislike";

        Map<String, String>  params = new HashMap<>();
        // the POST parameters:
        try {

            params.put("usuario_id", String.valueOf( comentario.getUsuario_id() ) );
            params.put("comentario_id", String.valueOf(comentario.getId()));

            final Comentario inner = comentario;

            HttpRequest.getInstance( getContext() ).somePostRequestReturningString(url, params, new HttpRequest.SomeCustomListener<String>() {
                @Override
                public void getResult(String result) {
                    if (!result.isEmpty()) {
                        try {
                            JSONObject obj = new JSONObject( result );
                            Log.e("RESULT", obj.toString() );
                            inner.setLikes( obj.getInt("likes") );
                            inner.setDislikes(obj.getInt("dislikes"));
                            inner.setLiked(obj.getBoolean("liked"));
                            inner.setDisliked(obj.getBoolean("disliked"));
                            //atualizar adapter
                            updateAdapter();
                            Toast.makeText(getContext(), "Você descurtiu o comentário", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }else{
                        Toast.makeText( getContext() , "Não foi possível descurtir o comentário", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch( Exception e){
            Toast.makeText( getContext() , "Erro ao conectar ao servidor.", Toast.LENGTH_SHORT).show();
        }
    }

    public void denunciar( Comentario comentario ){
        String url = "http://noiadigital.com.br/spotted/public/api/post/denunciar";

        Map<String, String>  params = new HashMap<>();
        // the POST parameters:
        try {
            params.put("usuario_id", String.valueOf( comentario.getUsuario_id() ) );
            params.put("comentario_id", String.valueOf(comentario.getId()));

            final Comentario inner = comentario;

            HttpRequest.getInstance( getContext() ).somePostRequestReturningString(url, params, new HttpRequest.SomeCustomListener<String>() {
                @Override
                public void getResult(String result) {
                    if (!result.isEmpty()) {
                        try {
                            JSONObject obj = new JSONObject( result );
                            inner.setDenunciado(true);
                            //atualizar adapter
                            updateAdapter();
                            Toast.makeText(getContext(), "Você denunciou o comentário", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }else{
                        Toast.makeText( getContext() , "Não foi possível denunciar o comentário", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch( Exception e){
            Toast.makeText( getContext() , "Erro ao conectar ao servidor.", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateAdapter(){
        this.notifyDataSetChanged();
    }

}
