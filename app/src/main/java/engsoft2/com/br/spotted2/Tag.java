package engsoft2.com.br.spotted2;

/**
 * Created by efd_net on 27/10/2015.
 */
public class Tag {

    private int id;
    private String tag;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
