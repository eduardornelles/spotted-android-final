package engsoft2.com.br.spotted2;

import java.util.ArrayList;

/**
 * Created by EduDornelles on 15/10/2015.
 */
public class Post {

    int id;
    Integer usuario_id;
    String texto;
    String imagemUrl;
    String data;
    int likes = 0;
    int dislikes = 0;
    int qteComentarios = 0;
    int denuncias = 0;
    boolean liked;
    boolean disliked;
    boolean denunciado;
    String tags;

    public String getImagemUrl() {
        return imagemUrl;
    }

    public void setImagemUrl(String imagemUrl) {
        this.imagemUrl = imagemUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getData() { return data;  }

    public void setData(String data) { this.data = data;  }

    public int getLikes() {  return likes;  }

    public void setLikes(int likes) { this.likes = likes; }

    public int getDislikes() { return dislikes; }

    public void setDislikes(int dislikes) { this.dislikes = dislikes; }

    public int getQteComentarios() {  return qteComentarios;}

    public void setQteComentarios(int comentarios) { this.qteComentarios = comentarios; }

    public int getDenuncias() { return denuncias;  }

    public void setDenuncias(int denuncias) {  this.denuncias = denuncias; }

    public boolean isDisliked() { return disliked;}

    public void setDisliked(boolean disliked) { this.disliked = disliked; }

    public boolean isLiked() { return liked; }

    public void setLiked(boolean liked) { this.liked = liked; }

    public String getTags() {  return tags; }

    public void setTags(String tags) {  this.tags = tags; }

    public boolean isDenunciado() { return denunciado; }

    public void setDenunciado(boolean denunciado) {  this.denunciado = denunciado; }

    @Override
    public String toString(){
        return getTexto();
    }

}
