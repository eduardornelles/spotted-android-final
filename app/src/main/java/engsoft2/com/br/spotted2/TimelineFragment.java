package engsoft2.com.br.spotted2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ListView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class TimelineFragment extends android.support.v4.app.Fragment {

    ListView feed_list;
    FeedAdapter adapter;
    ArrayList<Post> listaPosts;

    public TimelineFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate( R.menu.timeline, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_post:
                goToPostar();
                return true;
            case R.id.action_refresh:
                getPosts();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_logout:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed(){

        AlertDialog.Builder builder = new AlertDialog.Builder( getContext() );
        builder.setMessage("Tem certeza que deseja sair?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LoginManager.getInstance().logOut();

                        getFragmentManager().popBackStack();

                        getFragmentManager()
                                .beginTransaction()
                                .add(R.id.fragmentParentViewGroup, new LoginFragment())
                                .commit();

                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        builder.show();
    }

    public void goToPostar() {

        PostFragment postFragment = new PostFragment();

        getFragmentManager().beginTransaction()
                .replace(R.id.fragmentParentViewGroup, postFragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onCreate( Bundle savedInstance ){
        super.onCreate(savedInstance);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        getPosts();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_timeline, container, false);
        listaPosts = new ArrayList<>();
        adapter = new FeedAdapter(getContext() , listaPosts, this );
        feed_list = (ListView) v.findViewById( R.id.feed_list );
        feed_list.setDivider(null);
        feed_list.setAdapter(adapter);


        return v;
    }

    public void openComentarios( Post post){

        Bundle args = new Bundle();
        args.putString("post_id", String.valueOf( post.getId() ) );
        args.putString("usuario_id", String.valueOf(post.getUsuario_id()));

        ComentariosFragment comentariosFragment = new ComentariosFragment();
        comentariosFragment.setArguments(args);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragmentParentViewGroup, comentariosFragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onResume(){
        super.onResume();
        getPosts();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    onBackPressed();
                    return true;
                }
                return false;

            }
        });
    }

    public void getPosts(){

        final ProgressDialog ringProgressDialog = ProgressDialog.show(getContext(), "Aguarde ...",	"Carregando posts ...", true);
        ringProgressDialog.setCancelable(true);

        String url = "http://noiadigital.com.br/spotted/public/api/get_feed";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Result handling

                        try {

                            adapter.clear();

                            JSONObject json = new JSONObject( response );
                            String status = json.getString("status");
                            JSONArray posts = json.getJSONArray("data");

                            listaPosts.clear();

                            for (int i = 0; i < posts.length(); i++) {
                                JSONObject post = posts.getJSONObject(i);
                                Integer id = (int) post.get("id");
                                int usuario_id = post.getInt("usuario_id");
                                String texto = post.getString("texto");
                                String imagem = post.getString("imagem");
                                String data = post.getString("created_at");
                                int likes = post.getInt("likes");
                                int dislikes = post.getInt("dislikes");
                                int qteComentarios = post.getInt("qteComentarios");
                                boolean liked = post.getBoolean("liked");
                                boolean disliked = post.getBoolean("disliked");
                                boolean denunciado = post.getBoolean("denunciado");
                                String tags = post.getString("post_tags");
                                Post p = new Post();
                                p.setId(id);
                                p.setUsuario_id(usuario_id);
                                p.setTexto(texto);
                                p.setData(data);
                                p.setLikes(likes);
                                p.setDislikes(dislikes);
                                p.setQteComentarios(qteComentarios);
                                p.setLiked(liked);
                                p.setDisliked(disliked);
                                p.setDenunciado(denunciado);
                                p.setTags( tags );
                                if( URLUtil.isValidUrl( imagem ) ){
                                    p.setImagemUrl(imagem);
                                }
                                listaPosts.add(p);
                            }

                            adapter.notifyDataSetChanged();

                            ringProgressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ringProgressDialog.dismiss();

                new AlertDialog.Builder(getContext())
                        .setTitle("Erro!")
                        .setMessage("Não foi possível conectar ao servidor")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                long login_id = sharedPref.getInt("login_id", 0);
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("usuario_id", String.valueOf( login_id) );
                return params;
            }
        };

        // Add the request to the queue
        Volley.newRequestQueue( getContext() ).add(stringRequest);

    }



}
