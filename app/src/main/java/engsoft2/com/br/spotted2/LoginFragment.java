package engsoft2.com.br.spotted2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LoginFragment extends android.support.v4.app.Fragment {

    CallbackManager callbackManager;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;
    JSONObject retorno;

    private FacebookCallback<LoginResult> callBack = new FacebookCallback<LoginResult>() {

                @Override
        public void onSuccess(LoginResult loginResult) {

           sendDataLogin(loginResult);


        }

        @Override
        public void onCancel() {
            
        }

        @Override
        public void onError(FacebookException error) {
            
        }

    };

    public LoginFragment(){

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.login, menu);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        checkIsLogged();

        callbackManager = CallbackManager.Factory.create();
        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

            }
        };

        tokenTracker.startTracking();
        profileTracker.startTracking();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        checkIsLogged();

        return inflater.inflate(R.layout.fragment_login, container, false);
    }


    @Override
    public void onViewCreated( View view, Bundle savedInstance){
        super.onViewCreated(view, savedInstance);
        LoginButton loginButton = (LoginButton) view.findViewById( R.id.login_button );
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends, user_location, user_relationships"));
        loginButton.setFragment(this);
        loginButton.setLoginBehavior( LoginBehavior.NATIVE_WITH_FALLBACK );
        loginButton.registerCallback(callbackManager, callBack);

        checkIsLogged();

    }

    public void checkIsLogged(){

        AccessToken token = AccessToken.getCurrentAccessToken();
        if( token != null){

            getFragmentManager().beginTransaction()
                .replace(R.id.fragmentParentViewGroup, new TimelineFragment())
                .addToBackStack(null)
                .commit();

        }
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
      public void onStop(){
        super.onStop();
        tokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume(){
        super.onResume();

        Profile profile = Profile.getCurrentProfile();
    }

    public void sendDataLogin( LoginResult loginResult){

        GraphRequest request = GraphRequest.newMeRequest(
            loginResult.getAccessToken(),
            new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(
                        JSONObject object,
                        GraphResponse response) {

                    retorno = object;

                    try {
                        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putLong("login_id", retorno.getLong("id"));
                        editor.commit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                        Log.e("RETORNO FACE", retorno.toString() );

                        String url = "http://noiadigital.com.br/spotted/public/api/auth";

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // Result handling
                                        Log.e("ENVIOU POST USUARIO", retorno.toString());

                                        try {
                                            JSONObject respostaLogin = new JSONObject( response );
                                            Log.e("RESPOSTA SERVER", response.toString() );
                                            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPref.edit();
                                            editor.putInt("login_id", respostaLogin.getInt("login_id"));

                                            boolean showTermos = respostaLogin.getBoolean("showTermos");

                                            editor.commit();

                                            if( showTermos ){

                                                String titulo = respostaLogin.getString("termosTitulo");
                                                String texto = respostaLogin.getString("termosTexto");

                                                new AlertDialog.Builder(getContext())
                                                        .setTitle( titulo )
                                                        .setMessage( texto )
                                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                checkIsLogged();
                                                            }
                                                        })
                                                        .show();

                                            }else{

                                                checkIsLogged();
                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("DEU ERRO", error.toString());
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams()
                            {
                                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                                long login_id = sharedPref.getLong("login_id", 0);

                                Map<String, String>  params = new HashMap<>();
                                // the POST parameters:
                                try {
                                    params.put("facebook_id", String.valueOf( login_id ) );
                                    params.put("nome", retorno.getString("name"));
                                    params.put("email", retorno.getString("email"));
                                    params.put("dt_nascimento", retorno.getString("birthday"));
                                    params.put("relacionamento", retorno.getString("relationship_status"));
                                    params.put("cidade", retorno.getString("location"));
                                    params.put("amigos", retorno.getString("friends"));

                                    Log.e("PARAMS SEND", params.toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                return params;
                            }
                        };

                        Volley.newRequestQueue(getContext()).add(stringRequest);

                    }
                });


        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday, friends, location, relationship_status");
        request.setParameters(parameters);
        request.executeAsync();
    }


}
