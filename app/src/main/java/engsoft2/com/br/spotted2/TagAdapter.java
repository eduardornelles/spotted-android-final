package engsoft2.com.br.spotted2;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by EduDornelles on 15/10/2015.
 */
public class TagAdapter extends ArrayAdapter<Tag>{

    private LruCache<String, Bitmap> memoryCache;
    private HashMap<String, String> idPairs = new HashMap<String, String>();
    public PostFragment fragment;

    TagAdapter(Context context, ArrayList<Tag> tags){
        super( context, R.layout.tags_row, tags);
    }

    public void setFragment( PostFragment fragment ){
        this.fragment = fragment;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        LayoutInflater inflater = LayoutInflater.from(getContext());

        Tag tag = getItem(position);

        View customView = inflater.inflate(R.layout.tags_row, parent, false);
        CheckBox chk = (CheckBox) customView.findViewById( R.id.checkBoxTag );
        chk.setText(tag.getTag());
        chk.setId( tag.getId() );

        chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                if (checked) {
                    fragment.tagsMarcadas.add( String.valueOf(v.getId()) );
                } else{
                    fragment.tagsMarcadas.remove(String.valueOf(v.getId()));
                }

                Log.e("TAGS", fragment.tagsMarcadas.toString());
            }

        });

        return customView;

    }

}
