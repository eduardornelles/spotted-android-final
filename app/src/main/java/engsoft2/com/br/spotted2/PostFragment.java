package engsoft2.com.br.spotted2;


import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends DialogFragment {

    View v;
    EditText texto;
    JSONObject tags;
    ArrayList<Tag> listaTags;
    ImageView imagem_postar;
    TagAdapter adapter;
    ListView lista_tags;
    public final String APP_TAG = "SpottedFeevale";
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    private static final int SELECT_PHOTO = 100;
    public String photoFileName = "photo.jpg";
    ArrayList<String> tagsMarcadas = new ArrayList<>();
    public static final int WIDTH_FOTO = 720;
    public static final int HEIGHT_FOTO = 480;

    public PostFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.post, menu);
    }

    @Override
    public void onCreate( Bundle savedInstance ){
        super.onCreate(savedInstance);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(texto, InputMethodManager.SHOW_IMPLICIT);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                goBack();
                return true;
            case R.id.action_send:
                doSend();
                return true;
            case R.id.action_pick:
                selectPhoto();
                return true;
            case R.id.action_picture:
                takeFoto();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    goBack();
                    return true;
                }
                return false;

            }
        });
    }


    public void goBack(){
        if( texto.getText().toString().trim().length() > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder( getContext() );
            builder.setMessage("Tem certeza que deseja cancelar a postagem?")
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    })
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });

            builder.show();
        }else{
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        View  v = inflater.inflate(R.layout.fragment_post, container, false);
        imagem_postar = (ImageView) v.findViewById( R.id.imagem_postar );
        texto = (EditText) v.findViewById( R.id.texto_post );

        getTags();

        listaTags = new ArrayList<>();
        adapter = new TagAdapter(getContext() , listaTags );
        lista_tags = (ListView) v.findViewById( R.id.lista_tags );
        lista_tags.setDivider(null);
        lista_tags.setAdapter(adapter);
        adapter.setFragment(this);

        return v;
    }


    public void doSend(){

        String url = "http://noiadigital.com.br/spotted/public/api/insert_post";

        if( texto.getText().toString().trim().length() == 0 && imagem_postar.getVisibility() == View.GONE ){
            new AlertDialog.Builder(getContext())
                    .setTitle("Atenção!")
                    .setMessage("Você deve informar um texto para postar.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    })
                    .show();
        }else {

            final ProgressDialog ringProgressDialog = ProgressDialog.show(getContext(), "Aguarde ...", "Enviando comentario ...", true);
            ringProgressDialog.setCancelable(true);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Result handling
                            try {
                                JSONObject json = new JSONObject(response);
                                /*if( json.getString("status") == "OK"){*/
                                getActivity().getSupportFragmentManager().popBackStack();
                                ringProgressDialog.dismiss();
                                Toast.makeText(getContext(), "Post enviado com sucesso", Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                ringProgressDialog.dismiss();
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    getActivity().getSupportFragmentManager().popBackStack();
                    ringProgressDialog.dismiss();
                    Toast.makeText(getContext(), "Post enviado com sucesso", Toast.LENGTH_SHORT).show();

                }
            }) {

                @Override
                protected Map<String, String> getParams() {

                    SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                    long login_id = sharedPref.getInt("login_id", 0);

                    Map<String, String> params = new HashMap<>();
                    params.put("usuario_id", String.valueOf( login_id ) );
                    params.put("texto", texto.getText().toString() );
                    JSONArray tagsJson = new JSONArray( tagsMarcadas  );
                    params.put("tags", tagsJson.toString() );
                    String imagem = "";

                    if( imagem_postar.getVisibility() == View.VISIBLE ){
                        BitmapDrawable drawable = (BitmapDrawable) imagem_postar.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        Bitmap resized = Bitmap.createScaledBitmap(bitmap, 720, 480, false);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        resized.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                        byte [] byte_arr = stream.toByteArray();
                        imagem = Base64.encodeToString( byte_arr, Base64.DEFAULT );
                    }
                    params.put("imagem", imagem);

                    return params;
                }
            };

            // Add the request to the queue
            Volley.newRequestQueue(getContext()).add(stringRequest);
        }
    }

    public void getTags(){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(getContext(), "Aguarde ...",	"Carregando ...", true);
        ringProgressDialog.setCancelable(true);
        String url = "http://noiadigital.com.br/spotted/public/api/get_tags";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Result handling
                        try {
                            tags = new JSONObject( response );

                            adapter.clear();

                            JSONObject json = new JSONObject( response );
                            String status = json.getString("status");
                            JSONArray tags = json.getJSONArray("data");

                            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putInt("tamanhoImagem", json.getInt("tamanhoImagem") );
                            editor.commit();

                            listaTags.clear();

                            for (int i = 0; i < tags.length(); i++) {
                                JSONObject post = tags.getJSONObject(i);
                                Tag tag = new Tag();
                                int id = post.getInt("id");
                                String descricao = post.getString("descricao");
                                tag.setId( id );
                                tag.setTag(descricao);
                                listaTags.add( tag );
                            }

                            adapter.notifyDataSetChanged();
                            ringProgressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            ringProgressDialog.hide();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ringProgressDialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle("Erro!")
                        .setMessage("Não foi possível conectar ao servidor.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .show();

            }
        });

        // Add the request to the queue
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }

    public void selectPhoto(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    public void takeFoto(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri(photoFileName)); // set the image file name
        // Start the image capture intent to take photo
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public Uri getPhotoFileUri(String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            File mediaStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), APP_TAG );

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
                Log.d( APP_TAG , "failed to create directory");
            }

            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        int tamanhoImagem = sharedPref.getInt("tamanhoImagem", 0);

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE ) {
            // Make sure the request was successful
            Log.e("REQUEST", String.valueOf( resultCode ) );
            if( resultCode == getActivity().RESULT_OK ){

                try {
                    Uri takenPhotoUri = getPhotoFileUri(photoFileName);
                    Bitmap takenImage = transformBitmap(getContext(), takenPhotoUri, "camera");
                    //Bitmap takenImage = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), takenPhotoUri);

                    if( checkImageSize(takenImage) == false ) {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Atenção!")
                                .setMessage("A imagem que você tentou enviar é maior do que " + tamanhoImagem + "kb.")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .show();

                    }else{
                        imagem_postar.setImageBitmap( takenImage );
                        imagem_postar.setVisibility(View.VISIBLE);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else { // Result was a failure
                Toast.makeText( getContext() , "Erro ao capturar imagem!", Toast.LENGTH_LONG).show();
            }
        }

        if( requestCode == SELECT_PHOTO ){
            if(resultCode == getActivity().RESULT_OK){
                Uri selectedImage = data.getData();

                try {
                    //InputStream imageStream = getContext().getContentResolver().openInputStream(selectedImage);
                    Bitmap yourSelectedImage = transformBitmap(getContext(), selectedImage, "file");

                    if( checkImageSize(yourSelectedImage) == false ){
                        new AlertDialog.Builder(getContext())
                                .setTitle("Atenção!")
                                .setMessage("A imagem que você tentou enviar é maior do que " + tamanhoImagem + "kb.")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .show();
                    }else{
                        imagem_postar.setImageBitmap(yourSelectedImage);
                        imagem_postar.setVisibility(View.VISIBLE);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public boolean checkImageSize( Bitmap bmp){

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        int tamanhoImagem = sharedPref.getInt("tamanhoImagem", 0);

        byte[] data = null;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        data = baos.toByteArray();

        long size = data.length;

        Log.e("IMAGEM", String.valueOf( size / 1000 ) );

        if( size / 1000 > ( tamanhoImagem  ) ){
            return false;
        }
        return true;
    }


    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    public static int getOrientation(Context context, Uri photoUri) {

        int rotationInDegrees = 0;
        try {
            ExifInterface exif = new ExifInterface( photoUri.getPath() );
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            rotationInDegrees = exifToDegrees(rotation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rotationInDegrees;
    }

    public static int getOrientationFromFile( Context context, Uri photoUri ){
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static Bitmap transformBitmap(Context context, Uri photoUri, String tipo) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = 0;
        if( tipo == "camera"){
            orientation = getOrientation(context, photoUri);
        }else{
            orientation = getOrientationFromFile(context, photoUri);
        }

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > WIDTH_FOTO || rotatedHeight > HEIGHT_FOTO) {
            float widthRatio = ((float) rotatedWidth) / ((float) WIDTH_FOTO);
            float heightRatio = ((float) rotatedHeight) / ((float) HEIGHT_FOTO);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

    /*
     * if the orientation is not 0 (or -1, which means we don't know), we
     * have to do a rotation.
     */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        return srcBitmap;
    }



}
