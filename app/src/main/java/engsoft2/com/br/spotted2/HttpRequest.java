package engsoft2.com.br.spotted2;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HttpRequest
{
    private static final String TAG = "HttpRequest";
    private static HttpRequest instance = null;
    private Context context;
    private JSONObject json;

    //for Volley API
    public RequestQueue requestQueue;

    private HttpRequest(Context context)
    {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        //other stuf if you need
    }

    public static synchronized HttpRequest getInstance(Context context)
    {
        if (null == instance)
            instance = new HttpRequest(context);
        return instance;
    }

    //this is so you don't need to pass context each time
    public static synchronized HttpRequest getInstance()
    {
        if (null == instance)
        {
            throw new IllegalStateException(HttpRequest.class.getSimpleName() +
                    " is not initialized, call getInstance(...) first");
        }
        return instance;
    }

    public interface SomeCustomListener<T>
    {
        public void getResult(T object);
    }

    public void somePostRequestReturningString(String url, Map<String, String> params, final SomeCustomListener<String> listener)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
            new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    if(null != response.toString())
                        listener.getResult(response.toString());
                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    if (null != error.networkResponse)
                    {
                        listener.getResult("false");
                    }
                }
            });

        Log.e("params", params.toString() );

        requestQueue.add(request);
    }
}